import React from 'react';
import {Link} from "react-router-dom";

const ACTION = '/services/store-data';

export default class LoadXmlForm extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            file: null,

            showSuccess: false,
            showError: false
        };

        this.submit = this.submit.bind(this);
    }

    hideStatusText = () => {
        this.setState(prevState => ({
            showSuccess: false,
            showError: false
        }));
    };

    setFile = (e) => {
        this.hideStatusText();

        const file = e.target.files[0];

        const reader = new FileReader();

        reader.onload = (e) => {
            let contents = e.target.result;
            this.setState(prevState => ({file: contents}));
        };

        reader.readAsText(file, 'windows-1251');
    };

    submit() {
        this.hideStatusText();

        const options = {
            method: 'POST',
            headers : new Headers({
                Accept: 'application/json',
                'content-type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest'
            }),
            credentials: 'same-origin',
            body: JSON.stringify(this.state)
        };

        return fetch(ACTION, options).then(jsonResponse => {
            if (jsonResponse.redirected) {
                window.location.href = jsonResponse.url;
                return;
            }

            if (jsonResponse.status !== 200) {
                return Promise.reject(jsonResponse);
            }

            return jsonResponse
                .json()
                .then(json => {
                    if (json.state === 1) {
                        this.setState(prevState => ({showSuccess: true}));
                    } else {
                        this.setState(prevState => ({showError: true}));
                    }
                })
                .catch(error => {
                    console.log(error);
                });
        });
    }

    render() {
        return (
            <form className='form' action="/services/store-data" method='POST'>
                <h3>Выберите xml с данными</h3>
                <input onChange={this.setFile} className='form-control-file' type='file' accept='text/xml'/>

                <div className="form-group">
                    {
                        this.state.showSuccess &&
                        <span className="status alert alert-success">Данные успешно загружены.</span>
                    }
                    {
                        this.state.showError &&
                        <span className="status alert alert-danger">Ошибка загрузки данных, подробности в логе.</span>
                    }

                    <button type='button' className='btn btn-primary' onClick={this.submit}>Отправить</button>
                </div>

                <Link to="/services/view">Открыть справочник</Link>
            </form>
        );
    }
}