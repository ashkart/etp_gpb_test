<?php


namespace Helper;

use Doctrine\ORM\EntityManager;
use Zend\ServiceManager\ServiceLocatorInterface;

trait TraitDataSource
{
    /**
     * @var ServiceLocatorInterface
     */
    public static $sm;

    /**
     * @var EntityManager
     */
    public static $em;

    /**
     * @var ServiceRegistry
     */
    public static $sr;

    static function setServiceManager(ServiceLocatorInterface $sm)
    {
        self::$sm = $sm;
        self::$em = self::$sm->get('doctrine.entitymanager.orm_default');
        self::$sr = $sm->get(Factory::SERVICE_REGISTRY);
    }
}