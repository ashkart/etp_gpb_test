<?php

namespace Model\Service;

use Helper\Factory;
use Helper\TraitDataSource;

class AbstractService
{
    use TraitDataSource;

    protected function _logException(\Exception $e)
    {
        self::$sm->get(Factory::ERROR_HANDLER)->logException($e, self::$sm->get(Factory::REQUEST));
    }
}