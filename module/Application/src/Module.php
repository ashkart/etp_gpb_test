<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Helper\Factory;
use Plugin\Params;
use Helper\TraitDataSource;
use Psr\Container\ContainerInterface;
use Zend\EventManager\EventInterface;
use Zend\Mvc\Controller\PluginManager;
use Zend\Mvc\MvcEvent;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\View;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    /**
     * @param MvcEvent | EventInterface $e
     */
    public function onBootstrap(EventInterface $e)
    {
        $app = $e->getApplication();
        $app->getEventManager()->attach('render', [$this, 'registerJsonStrategy'], 100);

        $request = $e->getRequest();

        $sm = $app->getServiceManager();

        TraitDataSource::setServiceManager($sm);

        try {
            $conn = TraitDataSource::$em->getConnection();
            $conn->getDatabasePlatform()->registerDoctrineTypeMapping('ltree', 'string');
        } catch (\Exception $exception) {
            $sm->get(Factory::ERROR_HANDLER)->logException($e, $request);
        }

        /** @var PluginManager $cpm */
        $cpm = $sm->get(PluginManager::class);
        $cpm->setFactory(Params::class, function (ContainerInterface $sm) use ($request) {
            return new Params($request);
        });
    }

    public function registerJsonStrategy(MvcEvent $e)
    {
        $app          = $e->getTarget();
        /** @var ServiceLocatorInterface $sm */
        $sm           = $app->getServiceManager();
        /** @var View $view */
        $view         = $sm->get('Zend\View\View');
        $jsonStrategy = $sm->get('ViewJsonStrategy');

        // Attach strategy, which is a listener aggregate, at high priority
        $jsonStrategy->attach($view->getEventManager(), 100);
    }
}
