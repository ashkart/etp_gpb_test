<?php

namespace Exception;

use Throwable;
use Zend\View\Model\ViewModel;

class HttpException extends \Zend\Mvc\Exception\RuntimeException
{
    public function __construct(int $code = 0, string $message = '', Throwable $previous = null)
    {
        if (!$message) {
            $message = $this->_message($code);
        }

        parent::__construct($message, $code, $previous);
    }

    /**
     * @var ViewModel | null
     */
    protected $_viewModel = null;

    public function getViewModel() : ? ViewModel
    {
        return $this->_viewModel;
    }

    public function setViewModel(? ViewModel $viewModel) : self
    {
        $this->_viewModel = $viewModel;
        return $this;
    }

    /**
     * @return mixed
     */
    protected function _message(int $code)
    {
        return '';
    }
}