<?php


namespace Helper;

use Model\Service\AbstractService;
use Model\Service\ServiceTypeService;
use Psr\Container\ContainerInterface;

class ServiceRegistry
{
    /**
     * @var ContainerInterface
     */
    protected $_sm;

    public function __construct(ContainerInterface $sm)
    {
        $this->_sm = $sm;
    }

    public function getService(string $className) : AbstractService
    {
        return $this->_sm->get($className);
    }

    /** @return ServiceTypeService */
    public function serviceTypeService() : AbstractService { return $this->getService(ServiceTypeService::class); }
}