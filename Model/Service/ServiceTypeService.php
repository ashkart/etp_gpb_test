<?php


namespace Model\Service;

use Model\Generated\ServiceType;

class ServiceTypeService extends AbstractService
{
    const XML_TARGET_NODE_NAME = 'array';

    const XML_FIELD_GLOBAL_ID = 'global_id';
    const XML_FIELD_RAZDEL    = 'Razdel';
    const XML_FIELD_NAME      = 'Name';
    const XML_FIELD_IDX       = 'Idx';
    const XML_FIELD_KOD       = 'Kod';
    const XML_FIELD_NOMDESCR  = 'Nomdescr';

    public function parseAndSave(string $xmlString) : bool
    {
        $xmlReader = new \XMLReader();
        $xmlReader->XML($xmlString);

        try {
            self::$em->beginTransaction();

            while ($xmlReader->read()) {
                if (
                    $xmlReader->name === self::XML_TARGET_NODE_NAME &&
                    $xmlReader->nodeType === \XMLReader::ELEMENT
                ) {
                    $serviceType = new ServiceType();

                    while (
                        $xmlReader->read() &&
                        $xmlReader->name !== self::XML_TARGET_NODE_NAME
                    ) {
                        if ($xmlReader->nodeType === \XMLReader::ELEMENT) {
                            switch ($xmlReader->name) {
                                case self::XML_FIELD_GLOBAL_ID:
                                    $serviceType->setGlobalId($xmlReader->readInnerXml());
                                    break;

                                case self::XML_FIELD_RAZDEL:
                                    $serviceType->setRazdel($xmlReader->readInnerXml());
                                    break;

                                case self::XML_FIELD_NAME:
                                    $serviceType->setName($xmlReader->readInnerXml());
                                    break;

                                case self::XML_FIELD_IDX:
                                    $serviceType->setIdx($xmlReader->readInnerXml());
                                    break;

                                case self::XML_FIELD_KOD:
                                    $serviceType->setKod($xmlReader->readInnerXml());
                                    break;

                                case self::XML_FIELD_NOMDESCR:
                                    $serviceType->setNomdescr($xmlReader->readInnerXml());
                                    break;
                            }
                        }
                    }

                    if (
                        $xmlReader->name === self::XML_TARGET_NODE_NAME &&
                        $xmlReader->nodeType === \XMLReader::END_ELEMENT &&
                        $serviceType->getGlobalId() !== null
                    ) {
                        self::$em->persist($serviceType);
                        self::$em->flush($serviceType);
                    }
                }
            }

            self::$em->commit();
        } catch (\Exception $exception) {
            self::$em->rollback();

            $this->_logException($exception);

            return false;
        }

        return true;
    }

    public function queryTree() : array
    {
        $queryResult = self::$em->getRepository(ServiceType::class)->findAll();
        return $this->_buildTree($queryResult);
    }

    protected function _buildTree(array $models) : array
    {
        $tree = [];

        /** @var ServiceType $model */
        foreach ($models as $model) {
            $keys = explode('.', $model->getKod());

            $this->_addNode($tree, $keys, $model);
        }

        $tree = $this->_nomalizeTreeStruct($tree);

        return $this->_makeChildren($tree);
    }

    protected function _addNode(array &$branch, array $keys, ServiceType $model)
    {
        $key = $keys[0];
        unset($keys[0]);

        if (count($keys) > 0) {
            $branch[$key] = $branch[$key] ?? [];
            unset($branch['isLeaf']);

            $this->_addNode($branch[$key], array_values($keys), $model);
        } else {
            $branch[$key] = array_merge([
                'id'     => $model->getId(),
                'text'   => mb_convert_encoding("{$model->getKod()}:{$model->getName()}", 'windows-1251'),
                'isLeaf' => true,
                'idx'    => $model->getIdx()
            ], $branch[$key] ?? []);
        }
    }

    protected function _nomalizeTreeStruct(array $tree) : array
    {
        if (!isset($tree['id'])) {
            $tree = array_values($tree);
        }

        foreach ($tree as &$node) {
            if (is_array($node)) {
                $node = $this->_nomalizeTreeStruct($node);
            }
        }

        return $tree;
    }

    /*
     * xD
     */
    protected function _makeChildren(array $tree) : array
    {
        foreach ($tree as $key => &$node) {
            if ($key !== 'children' && is_array($node)) {
                $node = $this->_makeChildren($node);

                if (!isset($tree['children'])) {
                    $tree['children'] = [];
                }

                $tree['children'][] = $node;
                unset($tree[$key]);
            }
        }

        return $tree;
    }
}