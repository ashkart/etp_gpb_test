<?php

namespace Exception;

class HttpClientException extends HttpException
{
    const BAD_REQUEST_400        = 400;
    const FORBIDDEN_403          = 403;
    const NOT_FOUND_404          = 404;
    const METHOD_NOT_ALLOWED_405 = 405;
    const GONE_410               = 410;
    const I_AM_A_TEAPOT_418      = 418;
    const LOCKED_423             = 423;
    const UPGRADE_REQUIRED_426   = 426;

    protected function _message(int $code)
    {
        switch ($code) {
            case self::BAD_REQUEST_400:
                return 'bad_request';
            case self::FORBIDDEN_403:
                return 'forbidden';
            case self::NOT_FOUND_404:
                return 'not found';
            case self::METHOD_NOT_ALLOWED_405:
                return 'method not allowed';
            case self::GONE_410:
                return 'gone';
            case self::I_AM_A_TEAPOT_418:
                return 'i am a teapot';
            case self::LOCKED_423:
                return 'locked';
            case self::UPGRADE_REQUIRED_426:
                return 'upgrade required';

            default:
                return '';
        }
    }
}