<?php
//Insert in config folder as file name cli-config.php

require_once "../vendor/autoload.php";

// bootstrap.php
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Console\ConsoleRunner;


// Create a simple "default" Doctrine ORM configuration for Annotations
$paths = array(__DIR__."/../Model/Entity");
$isDevMode = true;

$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode, null, null, false);

// database configuration parameters
$conn = array(
    'driver'    => 'pdo_pgsql',
    'host'      => '192.168.0.161',
    'port'      => '5432',
    'user'      => 'etp',
    'password'  => 'qwe123zxc',
    'dbname'    => 'etp_test',
    'charset'   => 'utf8'
);

try {
    $entityManager = EntityManager::create($conn, $config);

    return ConsoleRunner::createHelperSet($entityManager);
} catch (\Exception $exception) {
    print $exception->getMessage();
}
