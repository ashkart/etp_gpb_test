<?php


namespace Application\Controller;

use Exception\HttpClientException;
use Zend\View\Model\JsonModel;

class ServiceTypeController extends AbstractController
{
    const POST_KEY_FILE = 'file';

    public function storeDataAction()
    {
        $this->_checkMethodPost();

        $data = $this->_fromPost();

        if (!isset($data[self::POST_KEY_FILE])) {
            throw new HttpClientException(HttpClientException::BAD_REQUEST_400, 'File is not set');
        }

        if (!self::$sr->serviceTypeService()->parseAndSave($data[self::POST_KEY_FILE])) {
            return new JsonModel([self::RESULT_STATE => self::RESULT_FAILURE]);
        }

        return new JsonModel([self::RESULT_STATE => self::RESULT_SUCCESS]);
    }

    public function loadTreeAction()
    {
        $this->_checkMethodGetOrHead();

        $tree = self::$sr->serviceTypeService()->queryTree();

        return new JsonModel($tree);
    }
}