<?php

namespace Helper;

use Zend\Log;

class Logger extends Log\Logger
{
    const LOG_PATH = APP_PATH . '/var/logs';

    private $_errorFilePath = null;

    public function setErrorFilePath(string $fileErrorPath = null)
    {
        $this->_errorFilePath = $fileErrorPath;
    }

    private function _addStreamWriter(string $fileName, Log\Filter\Priority $priority = null)
    {
        $writer = new Log\Writer\Stream($fileName);
        if ($priority !== null) {
            $writer->addFilter($priority);
        }

        $this->addWriter($writer);
    }

    /**
     * Инстанцируем Writers только при попытке записи,
     * до этого в разных частях приложения настройки (файлы записи) могут меняться.
     */
    public function log($priority, $message, $extra = [])
    {
        if ($this->_errorFilePath) {
            $this->_addStreamWriter($this->_errorFilePath, new Log\Filter\Priority(Log\Logger::WARN, '<='));
            $this->_errorFilePath = null; // Обнуляем, чтобы не добавлять при следующем вызове
        }

        return parent::log($priority, $message, $extra);
    }
}