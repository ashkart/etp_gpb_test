<?php

namespace Model\Generated;

use Doctrine\ORM\Mapping as ORM;

/**
 * ServiceType
 *
 * @ORM\Table(name="service_type", indexes={@ORM\Index(name="idx_gist_idx", columns={"idx"})})
 * @ORM\Entity
 */
class ServiceType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="service_type_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="global_id", type="bigint", nullable=false)
     */
    private $globalId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=200, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="razdel", type="string", length=10, nullable=false)
     */
    private $razdel;

    /**
     * @var string
     *
     * @ORM\Column(name="idx", type="string", nullable=false)
     */
    private $idx;

    /**
     * @var string
     *
     * @ORM\Column(name="kod", type="string", nullable=false)
     */
    private $kod;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nomdescr", type="string", length=2000, nullable=true)
     */
    private $nomdescr;



    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set globalId.
     *
     * @param int $globalId
     *
     * @return ServiceType
     */
    public function setGlobalId($globalId)
    {
        $this->globalId = $globalId;

        return $this;
    }

    /**
     * Get globalId.
     *
     * @return int
     */
    public function getGlobalId()
    {
        return $this->globalId;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return ServiceType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set razdel.
     *
     * @param string $razdel
     *
     * @return ServiceType
     */
    public function setRazdel($razdel)
    {
        $this->razdel = $razdel;

        return $this;
    }

    /**
     * Get razdel.
     *
     * @return string
     */
    public function getRazdel()
    {
        return $this->razdel;
    }

    /**
     * Set idx.
     *
     * @param string $idx
     *
     * @return ServiceType
     */
    public function setIdx($idx)
    {
        $this->idx = $idx;

        return $this;
    }

    /**
     * Get idx.
     *
     * @return string
     */
    public function getIdx()
    {
        return $this->idx;
    }

    /**
     * Set kod.
     *
     * @param string $kod
     *
     * @return ServiceType
     */
    public function setKod($kod)
    {
        $this->kod = $kod;

        return $this;
    }

    /**
     * Get kod.
     *
     * @return string
     */
    public function getKod()
    {
        return $this->kod;
    }

    /**
     * Set nomdescr.
     *
     * @param string|null $nomdescr
     *
     * @return ServiceType
     */
    public function setNomdescr($nomdescr = null)
    {
        $this->nomdescr = $nomdescr;

        return $this;
    }

    /**
     * Get nomdescr.
     *
     * @return string|null
     */
    public function getNomdescr()
    {
        return $this->nomdescr;
    }
}
