<?php

namespace Application\Controller;

use Helper\TraitDataSource;
use Exception\HttpClientException;
use Zend\Http\Request;
use Zend\Mvc\Controller\AbstractActionController;

/**
 * @property Request $request
 */
class AbstractController extends AbstractActionController
{
    use TraitDataSource;

    const RESULT_SUCCESS = 1;
    const RESULT_FAILURE = 0;

    const RESULT_STATE = 'state';

    public function __construct()
    {
        $this->layout('layout/layout.phtml');
    }

    protected function _fromPost($name = null, $default = null)
    {
        return $this->params()->fromPost($name, $default);
    }

    protected function _fromQuery($name = null, $default = null)
    {
        return $this->params()->fromQuery($name, $default);
    }

    protected function _checkMethodGetOrHead(bool $allowJsRequest = false)
    {
        $this->_checkMethod([Request::METHOD_GET, Request::METHOD_HEAD], $allowJsRequest);
    }

    protected function _checkMethodPost(bool $checkJsRequest = false)
    {
        $this->_checkMethod([Request::METHOD_POST], $checkJsRequest);
    }

    /**
     * @param string[] $methods
     */
    protected function _checkMethod(array $methods, bool $checkJsRequest = false)
    {
        if (!in_array($this->request->getMethod(), $methods)) {
            throw new HttpClientException(HttpClientException::METHOD_NOT_ALLOWED_405);
        }

        if ($checkJsRequest && !$this->request->isXmlHttpRequest()) {
            throw new HttpClientException(HttpClientException::BAD_REQUEST_400);
        }
    }
}