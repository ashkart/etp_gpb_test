<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

return [
    'doctrine' => [
        'connection' => [
            'orm_default' => [
                'driverClass' => Doctrine\DBAL\Driver\PDOPgSql\Driver::class,
                'params' => [
                    'host'     => 'localhost',
                    'user'     => 'app',
                    'port'     => 5432,
                    'password' => 'qwe',
                    'dbname'   => 'etp_test'
                ]
            ],
        ],

        'driver' => [
            'Model_driver' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [__DIR__ . '/../../Model/Generated'],
            ],

            'orm_default' => [
                'drivers' => [
                    "Model\\Generated\\" =>  'Model_driver'
                ],
            ],
        ],
    ],

    'logs' => [
        'error'   => 'var/logs/error.log',
    ],
];
