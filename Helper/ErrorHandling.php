<?php

namespace Helper;

use Zend\Log\PsrLoggerAdapter;
use Zend\Stdlib\ParametersInterface;
use Zend\Stdlib\RequestInterface;
use Zend\Http\PhpEnvironment\Request;
use Zend\Log\Logger;

class ErrorHandling
{
    use TraitDataSource;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @param PsrLoggerAdapter $logger
     */
    public function __construct($logger)
    {
        $this->logger = $logger->getLogger();
    }

    /**
     * @param Request | RequestInterface $request
     */
    public function logException(\Throwable $exception, Request $request)
    {
        $userAgent = $request->getHeader('USER_AGENT');
        $userAgent = $userAgent ? $userAgent->toString() : null;

        $log =
            "\n\nFile: {$exception->getFile()} ({$exception->getLine()})"
            . "\n\nmessage: "    . $exception->getMessage()
            . "\n\nreferer: "    . $request->getHeader('REFERER')
            . "\n\nrequest uri: ". $request->getRequestUri()
            . "\n\nuser agent: " . $userAgent
            . "\n\ntrace:\n"     . $exception->getTraceAsString()
            . "\n\nGET: "        . $this->_paramsToStringSafely($request->getQuery())
            . "\n\nPOST: "       . $this->_paramsToStringSafely($request->getPost())
            .  "\n\n*********************************************************************************";

        $this->logger->err($log);
    }

    protected function _paramsToStringSafely(ParametersInterface $params) : string
    {
        foreach ($params as $key => $param) {
            if (preg_match('~(passw|secret)~i', $key)) {
                $params[$key] = md5('ehslt' . $param);
            }
        }

        return $params->toString();
    }
}