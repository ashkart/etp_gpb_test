<?php

namespace Helper;

use Interop\Container\ContainerInterface;
use Zend\Log\PsrLoggerAdapter;
use Zend\ServiceManager\Factory\AbstractFactoryInterface;

class Factory implements AbstractFactoryInterface
{
    const CONFIG           = 'config';
    const SERVICE_REGISTRY = 'service_registry';
    const LOGGER           = 'app_logger';
    const ERROR_HANDLER    = 'app_error_handler';

    const REQUEST = 'Request';

    public function canCreate(ContainerInterface $container, $requestedName)
    {
        return array_key_exists($requestedName, $this->factories());
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return $this->factories()[$requestedName]($container);
    }

    protected function factories() : array
    {
        return [
            self::ERROR_HANDLER => function(ContainerInterface $sm) {

                /** @var PsrLoggerAdapter $logger */
                $logger  = $sm->get(self::LOGGER);
                $service = new ErrorHandling($logger);

                return $service;
            },

            self::LOGGER => function (ContainerInterface $sm) {
                $logger = new Logger();

                $logSettings = $sm->get(self::CONFIG)['logs'];

                $logger->setErrorFilePath($logSettings['error']);

                $log = new PsrLoggerAdapter($logger);
                return $log;
            },

            self::SERVICE_REGISTRY => function(ContainerInterface $sm) {
                return new ServiceRegistry($sm);
            }
        ];
    }
}