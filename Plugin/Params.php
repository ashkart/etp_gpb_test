<?php

namespace Plugin;

use Zend\Http\PhpEnvironment\Request;
use Zend\Mvc\Controller\Plugin\Params as ZendParams;
use Zend\Stdlib\Parameters;
use Zend\Stdlib\RequestInterface;

class Params extends ZendParams
{
    const CONTENT_TYPE_JSON = 'json';

    protected $_contentTypes = [
        self::CONTENT_TYPE_JSON => [
            'application/hal+json',
            'application/json'
        ]
    ];

    /**
     * @var Request
     */
    protected $_request;

    public function __construct(RequestInterface $request)
    {
        $this->_request = $request;
    }

    public function fromPost($param = null, $default = null)
    {
        $parentResult = parent::fromPost($param, $default);

        if (!$parentResult && $this->requestHasContentType(self::CONTENT_TYPE_JSON)) {
            $fromPost = (new Parameters(json_decode(file_get_contents('php://input'), true)))->toArray();

            switch (true) {
                case $param !== null:
                    return $fromPost[$param];
                case $default !== null:
                    return $fromPost[$param];
                default:
                    return $fromPost;
            }
        }

        return $parentResult;
    }

    public function requestHasContentType($contentType = '')
    {
        /** @var $headerContentType \Zend\Http\Header\ContentType */
        $headerContentType = $this->_request->getHeaders()->get('content-type');
        if (! $headerContentType) {
            return false;
        }

        $requestedContentType = $headerContentType->getFieldValue();
        if (false !== strpos($requestedContentType, ';')) {
            $headerData = explode(';', $requestedContentType);
            $requestedContentType = array_shift($headerData);
        }
        $requestedContentType = trim($requestedContentType);
        if (array_key_exists($contentType, $this->_contentTypes)) {
            foreach ($this->_contentTypes[$contentType] as $contentTypeValue) {
                if (stripos($contentTypeValue, $requestedContentType) === 0) {
                    return true;
                }
            }
        }

        return false;
    }
}