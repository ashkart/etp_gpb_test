const path = require('path');

const ENV_DEV  = 'development';
const ENV_PROD = 'production';

const ENV = process.env.ENV || ENV_DEV;

const webpack = require('webpack');

const TerserPlugin = require('terser-webpack-plugin');

let totalExports = [];

let commonConfig = {
    mode: ENV,
    devtool: ENV === ENV_DEV ? 'source-map' : false,

    context: path.resolve(__dirname, 'module/Application/js'),

    watch: ENV === ENV_DEV,
    watchOptions: {
        aggregateTimeout: 300
    },

    resolve: {
        modules: [
            'node_modules',
        ],
        extensions: ['.js', '.jsx']
    },
    resolveLoader: {
        modules: [
            'node_modules',
        ],
        extensions: ['.js']
    },

    module:{
        rules:[   //загрузчик для js/jsx
            {
                test: /\.jsx?$/, // определяем тип файлов
                loader: 'babel-loader',   // определяем загрузчик
                exclude: /(bower_compontents)/, // исключаем из обработки папку node_modules, это обязательно! */
                options:{
                    presets:['@babel/preset-react'],   // используемые плагины
                    plugins: [
                        '@babel/plugin-transform-runtime',
                        '@babel/plugin-proposal-object-rest-spread',
                        '@babel/plugin-proposal-class-properties'
                    ]
                }
            },
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader'
            }
        ]
    },

    optimization: {
        runtimeChunk: false,
        splitChunks : {
            cacheGroups: {
                vendor: {
                    test: '/[\\/]node_modules[\\/]/',
                    name: 'vendor',
                    chunks: 'all'
                }
            }
        },

        minimizer: []
    },

    plugins: [
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.EnvironmentPlugin('ENV')
    ]
};

if (ENV === ENV_PROD) {
    commonConfig.optimization.minimizer.push(
        new TerserPlugin({
            test: /\.js(\?.*)?$/i,
            cache: true,
            sourceMap: false,
            terserOptions: {
                ecma: 5,
                warnings: false,
                parse: {},
                compress: {},
                mangle: true, // Note `mangle.properties` is `false` by default.
                module: false,
                output: null,
                toplevel: false,
                nameCache: null,
                ie8: false,
                keep_classnames: undefined,
                keep_fnames: false,
                safari10: false,
            },
        })
    );
}

let moduleConfigs = {
    application: {
        entry: {
            index: './index'
        },
        output: {
            path: __dirname + '/public/js/application/',
            filename: '[name].js',
            library: '[name]'
        },
    }
};

for (let moduleName in moduleConfigs) {
    totalExports.push(Object.assign({}, commonConfig, moduleConfigs[moduleName]));
}

module.exports = totalExports;
