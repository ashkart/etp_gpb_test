import React from 'react';
import TreeView from 'deni-react-treeview';

export default class ViewDataTree extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            tree: []
        };
    }

    componentDidMount() {
        this.fetchData();
    }

    fetchData = () => {
        const options = {
            method: 'GET',
            headers : new Headers({
                Accept: 'application/json',
                'content-type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
            }),
            credentials: 'same-origin',
        };

        return fetch('/services/load-tree', options).then(jsonResponse => {
            if (jsonResponse.redirected) {
                window.location.href = jsonResponse.url;
                return;
            }

            if (jsonResponse.status !== 200) {
                return Promise.reject(jsonResponse);
            }

            return jsonResponse
                .json()
                .then(json => {
                    this.setState(prevState => ({tree: [json]}));
                })
                .catch(error => {
                    console.log(error);
                });
        });
    };

    filter = (e) => {
        const query = e.target.value;

        if (!query) {
            this.fetchData();
            return;
        }

        let data = Array.from(this.state.tree);

        const result = data.filter(function f(node) {
            if (node.text!== undefined && node.text.includes(query)) return true;

            if (node.children) {
                return (node.children = node.children.filter(f)).length
            }
        });

        this.setState(prevState => ({tree: result}));
    };

    render() {
        return (
            <div>
                <input type='text' onChange={this.filter} />
                <TreeView items={this.state.tree} />
            </div>
        );
    }
}
