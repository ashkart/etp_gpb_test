import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Switch, Route} from "react-router-dom";
import LoadXml from "./pages/loadXml";
import ViewDataTree from "./pages/viewDataTree";

class Application extends React.Component {
    render() {
        return (
            <Switch>
                <Route path="/services/view" component={ViewDataTree} />
                <Route path="/" component={LoadXml} />
            </Switch>
        );
    }
}

document.addEventListener('DOMContentLoaded', () => {
    ReactDOM.render(
        <BrowserRouter>
            <Application/>
        </BrowserRouter>,
        document.getElementById('app_root')
    );
});